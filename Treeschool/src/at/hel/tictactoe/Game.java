package at.hel.tictactoe;

import java.util.Scanner;

public class Game {

	private Scanner scan = new Scanner(System.in);
	private int[][] grid = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

	public void print() {

	}

	public void choose() {
		int rowSelection = getUserInput("Please chose your row of choice! (1-3)");
		int colSelection = getUserInput("Please chose your column of choice! (1-3)");
		grid[rowSelection][colSelection] = 1;
	}

	private int getUserInput(String question) {
		int tempInput;
		boolean falseInput;

		do {
			System.out.println(question);
			tempInput = scan.nextInt();
			if (tempInput <= 3 && tempInput >= 1) {
				System.out.println("You chose" + tempInput);
				falseInput = false;
			} else {
				falseInput = true;
				System.out.println("Please choose a number between 1 and 3");
			}

		} while (falseInput == true);
		return tempInput;
	}
}