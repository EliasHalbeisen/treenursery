package at.hel.algor;

import java.util.Arrays;
import java.util.stream.Collectors;

public class BubbleSort {
	
	public static void main(String[] args) {

		int[] array = { 45, 19, 3, 7};

		System.out.print("unsorted: ");
		printArray(array);

		System.out.print("sorted: ");
		sort(array);

		printArray(array);

	}

	private static void sort(int[] input) {
		sort(input, true);
	}

	private static void sort(int[] input, boolean sorted) {

		int inputLength = input.length;
		int temp;
		boolean is_sorted;

		for (int i = 0; i < inputLength; i++) {

			is_sorted = true;

			for (int j = 1; j < (inputLength - i); j++) {

				if (sorted) {
					if (input[j - 1] > input[j]) {
						temp = input[j - 1];
						input[j - 1] = input[j];
						input[j] = temp;
						is_sorted = false;
					}
				}
			}

			if (is_sorted)
				break;

		}

	}

	private static void printArray(int[] data) {
		String result = Arrays.stream(data).mapToObj(String::valueOf).collect(Collectors.joining(","));
		System.out.println(result);
	}

}
