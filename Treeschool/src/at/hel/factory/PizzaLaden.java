package at.hel.factory;

public class PizzaLaden {
   private EinfachePizzaFactory factory;

    public PizzaLaden(EinfachePizzaFactory factory) {
        this.factory = factory;
    }

    public Pizza OrderPizza (String pname){
        return factory.createPizza(pname);
    }
}
