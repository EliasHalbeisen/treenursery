package at.hel.factory;

import java.util.ArrayList;

public abstract class Pizza {
    protected String name;
    protected String teig;
    protected String sauce;
    protected ArrayList<String> Toppings;


    public Pizza() {

    }

    public String getName() {
        return name;
    }

    public void prepare(){

    }

    public void bake(){

    }

    public void cut(){

    }

    public void box(){

    }

    public String toString(){
        return "Ich bin eine Pizza";
    }
}
