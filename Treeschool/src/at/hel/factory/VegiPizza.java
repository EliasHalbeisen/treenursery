package at.hel.factory;

public class VegiPizza extends Pizza {


    public VegiPizza() {
        super();
        this.name="Vegi Pizza";
        this.teig="Pizzateig";
        this.sauce="Tomatensauce";
        this.Toppings.add("Kaese");
        this.Toppings.add("Pilze");
        this.Toppings.add("Basilikum");
    }
}
