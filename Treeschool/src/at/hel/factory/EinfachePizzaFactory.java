package at.hel.factory;

public class EinfachePizzaFactory {
    public EinfachePizzaFactory() {
    }
    public Pizza createPizza(String pname){
        if(pname=="VegiPizza"){
            return new VegiPizza();
        }
        else {
            return null;
        }
    }
}
