package at.hel.caesar;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
        String originalText = "ATTACKATONCE";
        int shiftCount = 1;
        System.out.println("Caesar Cipher Example");
        System.out.println("Encryption");
        System.out.println("Text  : " + originalText);
        System.out.println("Shift : " + shiftCount);
        String cipher = encrypt(originalText, shiftCount).toString();
        System.out.println("Encrypted Cipher: " + cipher);
        System.out.println("Decryption");
        System.out.println("Encrypted Cipher: " + cipher);
        System.out.println("Shift : " + shiftCount);
        String decryptedPlainText = decrypt(cipher, 26 - shiftCount).toString();
        System.out.println("Decrypted Plain Text  : " + decryptedPlainText);
    }


}