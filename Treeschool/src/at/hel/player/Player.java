package at.hel.player;

import java.util.ArrayList;
import java.util.List;

public class Player {

	private List<Song> Songs;
	

	public Player(List<Song> songs, String name) {
		super();
		Songs = songs;
		this.name = name;
	}

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void addSong(Song songs) {
		this.Songs.add(songs);
	}

	public Player(List<Actor> actors, List<Song> songs, String name) {
		super();

		this.Songs = new ArrayList<Song>(Songs);
		this.name = name;
	}

	public void playVideo() {

	}

	public void playSong() {

	}

	public void play() {

	}

	public void start() {

	}

	public void pause() {

	}

	public List<Song> getSongs() {
		return Songs;
	}

	public void setSongs(List<Song> songs) {
		Songs = songs;
	}

}
