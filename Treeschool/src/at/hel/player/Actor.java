package at.hel.player;

import java.util.List;

public class Actor {

	private String firstName;
	private String lastName;
	private List<CD> CDs;
	private List<DVD> DVDs;

	public Actor(String firstName, String lastName, List<CD> cDs, List<DVD> dVDs) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.CDs = cDs;
		this.DVDs = dVDs;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void addCD(CD CDS) {
		this.CDs.add(CDS);
	}

	public void addDVD(DVD DVDS) {
		this.DVDs.add(DVDS);
	}

}
