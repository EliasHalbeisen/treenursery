package at.hel.observer;

import java.util.ArrayList;
import java.util.List;

public class Sensor {

	public Sensor() {
		super();
		this.Observables = new ArrayList<Observable>();

	}

	private List<Observable> Observables;

	public void informAll() {
		for (Observable o : this.Observables) {
			o.inform();
		}
	}

	public List<Observable> getObservables() {
		return Observables;
	}

	public void setObservables(List<Observable> observables) {
		Observables = observables;
	}

	public void addObservable(Observable Obs) {
		this.Observables.add(Obs);
	}

}
