package at.hel.observer;

public class Main {

	public static void main(String[] args) {

		Sensor sensor = new Sensor();

		Lantern l1 = new Lantern();
		Lantern l2 = new Lantern();
		Christmastree c1 = new Christmastree();
		Trafficlight t1 = new Trafficlight();

		sensor.addObservable(l1);
		sensor.addObservable(l2);
		sensor.addObservable(c1);
		sensor.addObservable(t1);
		sensor.informAll();

	}

}
