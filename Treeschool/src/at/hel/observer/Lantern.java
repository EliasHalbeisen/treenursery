package at.hel.observer;

public class Lantern implements Observable {

	public void inform() {
		System.out.println("I was informed!");
	}

}
